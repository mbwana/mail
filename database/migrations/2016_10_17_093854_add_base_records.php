<?php

use Faker\Provider\Uuid;
use Illuminate\Database\Migrations\Migration;

class AddBaseRecords extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                DB::table('base_templates')->insert(
                    [
                        [
                            'reference' => (string)Uuid::uuid(),
                            'name' => 'Default',
                            'slug' => \App\BaseTemplate::DEFAULT_SLUG,
                            'created_at' => DB::raw('NOW()'),
                            'updated_at' => DB::raw('NOW()')
                        ]
                            ,
                        [
                            'reference' => (string)Uuid::uuid(),
                            'name' => 'LSHTM',
                            'slug' => 'lshtm',
                            'created_at' => DB::raw('NOW()'),
                            'updated_at' => DB::raw('NOW()')
                        ]

                    ]

                );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                DB::table('base_templates')->truncate();
        }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('messages', function (Blueprint $table) {
                        $table->increments('id');
                        $table->uuid('reference')->unique();
                        $table->string('subject');
                        $table->string('from');
                        $table->string('to');
                        $table->string('cc')->nullable();
                        $table->string('bcc')->nullable();
                        $table->text('message');
                        $table->integer('template');
                        $table->timestamps();
                        $table->softDeletes();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('messages');
        }
}

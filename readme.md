## LSHTM Mail Framework

[![Build Status](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/build.png?b=master&s=f26a9de7ca967befa59d66330c6b2328fc8c9cbd)](https://scrutinizer-ci.com/b/lshtmweb/mail/build-status/master)
[![Code Coverage](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/coverage.png?b=master&s=26bed08eed076bf6a586bf6d9e37178858935edd)](https://scrutinizer-ci.com/b/lshtmweb/mail/?branch=master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/quality-score.png?b=master&s=f4b8a52c2d1e1e1eb40b8cec4edd4c372c1f3aad)](https://scrutinizer-ci.com/b/lshtmweb/mail/?branch=master)

This component allows applications to send email via a simple API command. It works with the authentication server to confirm an application is able to carry out the proposed task.

This is part of the LSHTMWeb OpenSource Programme

## Official Documentation

Documentation coming soon.

## Security Vulnerabilities

If you discover a security vulnerability within this application, please send an e-mail to the web team at webeditor@lshtm.ac.uk. All security vulnerabilities will be promptly addressed.

### License

The license for this application is still pending

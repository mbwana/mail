<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 20/11/2017
 * Time: 18:27
 */

return [
    'messages' => [
        'email_sent_successfully' => "Email sent successfully",
        'problem_sending_email' => "Problem delivering email.",
        'problem_updating_email_status' => "Could not update email status.",
        'email_attempt' => "Attempting to send email.",
    ]
];
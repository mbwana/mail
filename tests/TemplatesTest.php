<?php

class TemplatesTest extends TestCase
{
        use \Illuminate\Foundation\Testing\DatabaseTransactions;
        private $templates;
        private $defined_template;
        private $invalid_template;
        private $update_template;

        public function setUp()
        {
                parent::setUp();
                $this->templates = new \App\Template();
                $this->defined_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World'
                ];
                $this->update_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World'
                ];
                $this->invalid_template = [
                    'title' => 'Test Template',
                    'type'  => 1,
                ];
        }

        /**
         * A basic test example.
         *
         * @return void
         */
        public function testGetTemplates()
        {
                $this->visit('/templates')
                    ->isFalse();
        }

        /**
         * Create a template and check for the title in the result.
         */
        public function testCreateTemplate()
        {
                $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_OK)
                    ->see($this->defined_template['title']);
        }

        public function testCreateTemplateValidation()
        {
                $this->post('/templates', $this->invalid_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        public function testUpdateTemplate()
        {
                $templateRequest = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $template = json_decode($templateRequest->response->content());
                $this->put("/templates/" . $template->results[0]->id, $this->update_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_OK)
                    ->see($this->update_template['title']);
        }

        public function testDeleteTemplate()
        {
                $templateRequest = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $template = json_decode($templateRequest->response->content());
                $this->delete("/templates/" . $template->results[0]->id, $this->update_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_OK)
                    ->see($this->update_template['title']);
        }
}

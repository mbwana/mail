<?php

class RoutesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomeInfo()
    {
        $this->visit('/')
             ->see('London School of Hygiene and Tropical Medicine - Mail API');
    }
}

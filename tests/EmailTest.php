<?php

/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 11/05/2016
 * Time: 10:11
 */
class EmailTest extends TestCase
{
        use \Illuminate\Foundation\Testing\DatabaseTransactions;
        private $templates;
        private $defined_template;
        private $invalid_template;
        private $update_template;

        public function setUp()
        {
                parent::setUp();
                $this->templates = new \App\Template();
                $this->defined_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World'
                ];
                $this->update_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World'
                ];
                $this->invalid_template = [
                    'title' => 'Test Template',
                    'type'  => 1,
                ];
        }

        public function testSendMail()
        {
                
        }
}
<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 16:57
 */

namespace App;

use Illuminate\Support\Facades\Log;

trait ParsesText
{

        /**
         * Parse selected text given certain parameters.
         * @param $params array An array of parameters.
         * @param $text string The string you want to parse.
         * @return string The parsed string.
         */
        public function parseText($params, $text)
        {
                $delimeter = config('app.start_delimeter');
                $const_scheme = '/(\\' . $delimeter . ')((?:[a-z][a-z0-9_]*))/is';
                preg_match_all($const_scheme, $text, $parameters);
                foreach ($parameters[0] as $parameter) {
                        $sparam = ltrim($parameter, $delimeter);
                        if (array_has($params, $sparam)) {
                                $value = array_get($params, $sparam);
                                $text = str_replace($parameter, $value, $text, $replacement_count);
                                Log::info(trans('app.message_parameter_replacement_made'));
                        } else {
                                Log::debug(trans('app.message_parameter_not_found'));
                        }
                }
                return $text;
        }


}
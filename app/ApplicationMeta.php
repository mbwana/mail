<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 11:14
 */

namespace App;


class ApplicationMeta extends BaseModel
{

        protected $table = "applications_meta";

        public function application()
        {
                return $this->belongsTo('App\Application', 'application_id');
        }
}
<?php

/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 03/05/2016
 * Time: 11:41
 */

namespace App;

class Template extends BaseModel
{
        protected $hidden = ['id', 'application'];

        protected $touches = ['application'];

        const DEFAULT_BASE = 2;

        public function application()
        {
                return $this->belongsTo('App\Application', 'application_id');
        }

        public function uses()
        {
                return $this->belongsTo('App\BaseTemplate', 'base', 'id');
        }
}
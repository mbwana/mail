<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 16:50
 */

namespace App;

class Message extends BaseModel
{

        /**
         * Status when the message has been queued.
         */
        const STATUS_QUEUED = 1;
        /**
         * Status when the message has been sent.
         */
        const STATUS_SENT = 2;
        /**
         * Status when the message has failed to send.
         */
        const STATUS_FAILED = 3;

        /**
         * Get related template record for this message.
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function template()
        {
                return $this->belongsTo('App\Template', 'template_id');
        }

        /**
         * Get all related attachments to this message.
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function attachments()
        {
                return $this->hasMany('App\Attachment', 'message_id');
        }
}
<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 12:36
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
        use UUIDGenerate;

        protected $hidden = ['id'];

}
<?php

/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 03/05/2016
 * Time: 11:41
 */

namespace App;

class BaseTemplate extends BaseModel
{
        use HasSlug;
        protected $hidden = ['id', 'application'];

        const DEFAULT_SLUG = 'html';

        public function provisions()
        {
                return $this->hasMany('App\Template', 'base');
        }
}
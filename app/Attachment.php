<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 16:50
 */

namespace App;

class Attachment extends BaseModel
{

        public function message()
        {
                return $this->belongsTo('App\Message', 'message_id');
        }
}
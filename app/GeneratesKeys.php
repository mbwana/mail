<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 11:57
 */

namespace App;


trait GeneratesKeys
{
        public static function bootGeneratesKeys()
        {
                static::creating(function ($model) {
                        $keys = self::generateKeys();
                        $model->key = $keys["key"];
                        $model->secret = $keys["secret"];
                });
        }


        /**
         * Generate a key pair.
         * @return array
         */
        protected static function generateKeys()
        {
                $res = openssl_pkey_new();

                openssl_pkey_export($res, $privkey);

                $pubkey = openssl_pkey_get_details($res);
                $pubkey = sha1($pubkey["key"]);
                $privkey = sha1($privkey);

                $keys = [
                    "key" => $pubkey,
                    "secret" => $privkey
                ];

                return $keys;
        }
}
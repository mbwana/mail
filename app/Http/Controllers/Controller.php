<?php

namespace App\Http\Controllers;

use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
        protected function respond($data, $status = Response::HTTP_OK)
        {
                $r = $_SERVER['REQUEST_TIME_FLOAT'];
                $requestTime = microtime(true) - $r;
                $data = [
                    "results" => $data,
                    "meta"    => [
                        "count"        => sizeof($data),
                        "about"        => config('app.name', 'Web Team Application'),
                        "request_time" => round($requestTime, 4, PHP_ROUND_HALF_UP)
                    ]
                ];
                return new JsonResponse($data, $status);
        }

        protected function error($data, $status = Response::HTTP_OK)
        {
                $r = $_SERVER['REQUEST_TIME_FLOAT'];
                $requestTime = microtime(true) - $r;
                $data = [
                    "errors" => $data,
                    "meta"    => [
                        "status"       => $status,
                        "about"        => config('app.name', 'Web Team Application'),
                        "request_time" => round($requestTime, 4, PHP_ROUND_HALF_UP)
                    ]
                ];
                return new JsonResponse($data, $status);
        }

        protected function buildFailedValidationResponse(Request $request, array $errors)
        {
                if ($request->ajax() || $request->wantsJson()) {
                        return $this->error($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                return redirect()->to($this->getRedirectUrl())
                    ->withInput($request->input())
                    ->withErrors($errors, $this->errorBag());
        }
}

<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 11:55
 */

namespace App\Http\Controllers;


use App\Application;
use App\GeneratesKeys;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{

        use GeneratesKeys;
        protected $application;

        public function __construct(Application $application)
        {
                $this->application = $application;
        }

        public function store(Request $request)
        {
                $this->validate($request, [
                    "name" => 'required|unique:applications,title'
                ]);
                $application = $this->application;
                $application->title = $request->name;
                try {
                        DB::transaction(function () use ($application) {
                                $application->save();
                        });
                } catch (\Exception $e) {
                        abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
                }
                return $this->respond($application);
        }
}
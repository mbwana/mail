<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 13:41
 */

namespace App\Http\Controllers;


use App\Application;
use App\Attachment;
use App\Jobs\SendEmail;
use App\Message;
use App\ParsesText;
use App\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EmailController extends MessageController
{
        use ParsesText;

        /**
         * @var Message
         */
        protected $mail_message;

        /**
         * Message Identifier.
         */

        /**
         * EmailController constructor.
         *
         * @param Message $email
         */
        public function __construct(Message $email)
        {
                $this->mail_message = $email;
        }

        public function store(Request $request)
        {
                $this->validate($request, [
                    'template'      => 'required',
                    'recipient'     => 'required',
                    'params'        => 'sometimes',
                    'key'           => 'required',
                    'from'          => 'sometimes|string',
                    'attachments[]' => 'sometimes|mimes:jpeg,png,pdf,doc,docx,zip'
                ]);
                $template = Template::where('reference', $request->template)->orWhere('key', $request->template)->first();
                if (empty($template)) {
                        abort(Response::HTTP_BAD_REQUEST, "Template not found.");
                }
                /** @var Application $application */
                $application = Application::where('key', trim($request->key))->first();
                if (empty($application)) {
                        abort(Response::HTTP_BAD_REQUEST, "Application not found");
                }
                if ($template->application_id != $application->id) {
                        Log::critical(trans('app.unauthorized_email_attempt'));
                        abort(Response::HTTP_UNAUTHORIZED, "This application is not allowed to send emails using this template.");
                }
                $mail = $this->mail_message;
                $mail->to = $request->recipient;
                $request->has('params') ? $parameters = $request->params : false;
                if ($request->has('cc')) {
                        $mail->cc = $request->cc;
                }
                if ($request->has('bcc')) {
                        $mail->bcc = $request->bcc;
                }
                count($application->meta) ? $mail->from = serialize(["email" => $application->meta->from_email, "name" => $application->meta->from_name]) :
                    $mail->from = serialize(["email" => config('app.mail'), "name" => config('app.name')]);

                $mail->subject = $template->title;
                $mail->template_id = $template->id;
                //TODO: Allow from to come from the applications configuration
                if (isset($parameters)) {
                        $parameters = explode(config('app.delimeter'), $parameters);
                        $newParams = new Collection;
                        foreach ($parameters as $parameter) {
                                list($k, $v) = explode('=', $parameter, 2);
                                $newParams->put($k, $v);
                        }
                        $mail->message = $this->parseText($newParams->toArray(), $template->content);
                        $mail->subject = $this->parseText($newParams->toArray(), $template->title);
                } else {
                        $mail->message = $template->content;
                }
                $job = (new SendEmail($mail));
                DB::transaction(function () use ($mail, $job, $request) {
                        $mail->status = Message::STATUS_QUEUED;
                        $mail->save();
                        try {
                                if ($request->hasFile('attachments')) {
                                        $files = $request->file('attachments');
                                        foreach ($files as $file) {
                                                $attachment = $this->process_file($file, $mail);
                                                if ($attachment != null) {
                                                        $mail->attachments()->save($attachment);
                                                }
                                        }
                                }
                                $this->dispatch($job);
                        } catch ( \Exception $e ) {
                                $this->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
                        }
                });

                $mail->addHidden(['template']);
                $mail->sent_via = $application->title;

                return $this->respond($mail);
        }

        /**
         * Process an uploaded file.
         *
         * @param UploadedFile $file
         * @param Message      $message
         *
         * @return Attachment|null
         */
        protected function process_file(UploadedFile $file, Message &$message)
        {
                if ($file->isValid()) {
                        $attachment = new Attachment;
                        $attachment->file_name = strtoupper(sha1(base64_encode($message->reference . Carbon::now()->timestamp)) . "_" . $file->getClientOriginalName());
                        $storage_folder = $message->template->application->getStoragePath();
                        $file = $file->move($storage_folder, $attachment->file_name);
                        $attachment->file_location = $storage_folder . DIRECTORY_SEPARATOR . $file->getFilename();

                        return $attachment;
                } else {
                        Log::error("Failed to save attachment on filesystem.");

                        //throw new BadRequestHttpException("Failed to attach file to message.");
                        return null;
                }

        }
}
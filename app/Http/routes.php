<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'MainController@getVersion');

$app->group(['prefix' => 'templates'], function ($app) {
        $app->get('/', 'App\Http\Controllers\TemplateController@index');
        $app->get('/{id}', 'App\Http\Controllers\TemplateController@show');
        $app->post('/', 'App\Http\Controllers\TemplateController@store');
        $app->put('/{id}', 'App\Http\Controllers\TemplateController@update');
        $app->delete('/{id}', 'App\Http\Controllers\TemplateController@destroy');
});

$app->group(['prefix' => 'applications'], function ($app) {
        //$app->get('/', 'App\Http\Controllers\TemplatesController@index');
        //$app->get('/{id}', 'App\Http\Controllers\TemplatesController@show');
        $app->post('/', 'App\Http\Controllers\ApplicationController@store');
});

$app->group(['prefix' => 'email'], function ($app) {
        $app->post('/', 'App\Http\Controllers\EmailController@store');
});
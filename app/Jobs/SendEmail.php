<?php

namespace App\Jobs;

use App\Attachment;
use App\BaseTemplate;
use App\Message;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;

class SendEmail extends Job implements SelfHandling, ShouldQueue
{
        use InteractsWithQueue, SerializesModels;

        protected $message;

        /**
         * SendEmail constructor.
         * @param Message $message
         */
        public function __construct(Message $message)
        {
                $this->message = $message;
        }

        /**
         * Execute the job
         * @param Mailer $mailer
         */
        public function handle(Mailer $mailer)
        {
                Log::info(trans('app.messages.email_attempt'));
                DB::beginTransaction();
                try {
                        //If the template has base template parameter set, use it, else use the default.
                        $this->message->template->uses->slug ? $baseTemplate = $this->message->template->uses->slug : $baseTemplate = BaseTemplate::DEFAULT_SLUG;
                        /** @var \Intervention\Image\Image $logo */
                        $logo = Image::cache(function($image) {
                                return $image->make(urldecode("https://publicapis.lshtm.ac.uk/global/logo?width=170"));
                        }, 7200);
                        $variables = [
                            'template' => $this->message->message,
                            'subject' => $this->message->subject,
                            'logo' => $logo,
                            'logo_name' => "school_seal.png",
                            'application_name' => $this->message->template->application->title,
                            'signature' => !empty($this->message->template->application->meta->signature) ? $this->message->template->application->meta->signature : config('app.name')
                        ];
                        $mailer->send('emails.' . $baseTemplate, $variables, function ($m) {
                                $sender = unserialize($this->message->from);
                                $m->from($sender["email"], $sender["name"]);
                                $m->to($this->message->to)->subject($this->message->subject);
                                if(count($this->message->attachments)) {
                                        $attachments = $this->message->attachments;
                                        $attachments->each(function($attachment) use ($m) {
                                                /** @var Attachment $attachment */
                                                $m->attach($attachment->file_location);
                                        });
                                }
                        });
                        $this->message->status = Message::STATUS_SENT;
                        $this->message->save();
                        DB::commit();
                        Log::info(trans('app.messages.email_sent_successfully'));
                } catch (\Exception $e) {
                        $this->message->status = Message::STATUS_FAILED;
                        $this->message->save();
                        DB::rollBack();
                        Log::error(trans('app.messages.problem_sending_mail') . " because {$e->getMessage()} at line: {$e->getLine()}" );
                }
        }

        public function failed(\Exception $e)
        {
                DB::transaction(function () use ($e) {
                        $this->message->status = Message::STATUS_FAILED;
                        $this->message->save();
                });
                Log::critical(trans('app.messages.problem_sending_mail') . " because {$e->getMessage()} at line: {$e->getLine()}" );
                Log::error(trans('app.messages.problem_updating_email_status') . "because " . $e->getMessage());
        }
}
